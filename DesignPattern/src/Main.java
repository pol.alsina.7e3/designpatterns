import java.awt.*;
import figuraGeometrica.Circulo;
import figuraGeometrica.Cuadrado;
import figuraGeometrica.Rectangulo;
import composite.Composite;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Circulo c1 = new Circulo("Parra", Color.BLUE, 3);
        Circulo cCopia = (Circulo) c1.clone();
        cCopia.setNombre("Clone");
        System.out.println(c1);
        System.out.println(cCopia);


        Rectangulo r1 = new Rectangulo("Alonso",Color.cyan,3,5);
        Rectangulo rCopia = (Rectangulo) r1.clone();
        System.out.println(r1);
        System.out.println(rCopia);

        Cuadrado cuadrado1 = new Cuadrado("32",Color.cyan,3);
        Cuadrado cuadradoClone = (Cuadrado) cuadrado1.clone();
        System.out.println(cuadrado1);
        System.out.println(cuadradoClone);

        System.out.println("composite");

        Composite composite = new Composite();
        Composite compositeClones = new Composite();
        composite.add(c1);
        composite.add(r1);
        composite.add(cuadrado1);
        compositeClones.add(cCopia);
        compositeClones.add(rCopia);
        compositeClones.add(cuadradoClone);

        System.out.println(composite.calculateArea());
        System.out.println(compositeClones.calculateArea());
    }
}
