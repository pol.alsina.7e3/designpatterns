package composite;

import java.util.ArrayList;
import java.util.List;

public class Composite implements Component{

    private List<Component> children = new ArrayList<>();

    public void add(Component component) {
        children.add(component);
    }

    public void remove(Component component) {
        children.remove(component);
    }

    @Override
    public String calculateArea() {
        StringBuilder result = new StringBuilder();
        for (Component component : children) {
            result.append(component.calculateArea() + "\n");
        }
        return result.toString();
    }
}
