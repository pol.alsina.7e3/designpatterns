package composite;

import figuraGeometrica.Circulo;
import figuraGeometrica.Cuadrado;
import figuraGeometrica.Rectangulo;

import java.awt.*;

public class MainComposite {
    public static void main(String[] args) {
        Circulo c1 = new Circulo("Parra", Color.BLUE, 3);
        Rectangulo r1 = new Rectangulo("Alonso",Color.cyan,3,5);
        Cuadrado cuadrado1 = new Cuadrado("32",Color.cyan,3);

        Composite composite = new Composite();
        composite.add(c1);
        composite.add(r1);
        composite.add(cuadrado1);

        System.out.println(composite.calculateArea());
    }
}
