package figuraGeometrica;

import prototype.Prototype;

import java.awt.*;

public class Circulo extends FiguraGeometrica {
    protected double radio;

    public Circulo(String nombre, Color color, double radio) {
        super(nombre, color);
        this.radio = radio;
    }
    private Circulo(Circulo toCopy){
        super(toCopy.nombre, toCopy.color);
        this.radio = toCopy.radio;
    }

    @Override
    public Prototype clone() {
        return new Circulo(this);
    }

    @Override
    public String calculateArea() {
        double area = Math.pow(radio,2) * 3.14;
        return "El area del circulo " + nombre + " es: " + area;
    }

    @Override
    public String toString() {
        return "Circulo{" +
                "nombre='" + nombre + '\'' +
                ", color=" + color +
                ", radio=" + radio +
                '}';
    }
}
