package figuraGeometrica;

import prototype.Prototype;

import java.awt.*;

public class Cuadrado extends FiguraGeometrica{

    protected double lado;

    public Cuadrado(String nombre, Color color, double lado) {
        super(nombre, color);
        this.lado = lado;
    }
    public Cuadrado(Cuadrado clone){
        super(clone.nombre,clone.color);
        this.lado = clone.lado;
    }

    @Override
    public String calculateArea() {
        double area = (Math.pow(lado,2));
        return "El area del cuadrado " + nombre + " es: " + area;
    }

    @Override
    public Prototype clone() {
        return new Cuadrado(this);
    }

    @Override
    public String toString() {
        return "Cuadrado{" +
                "lado=" + lado +
                ", nombre='" + nombre + '\'' +
                ", color=" + color +
                '}';
    }
}
