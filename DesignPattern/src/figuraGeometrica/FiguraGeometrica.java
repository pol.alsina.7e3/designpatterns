package figuraGeometrica;
import composite.Component;
import prototype.Prototype;
import java.awt.*;

public abstract class FiguraGeometrica implements Prototype, Component {

    protected FiguraGeometrica(String nombre, Color color) {
        this.nombre = nombre;
        this.color = color;
    }

    protected String nombre;

    protected Color color;


    public void setNombre(String nombreCambio) {
        this.nombre = nombreCambio;
    }


    public String getNombre() {
        return nombre;
    }


    public void setColor(Color color) {
        this.color = color;
    }


    public Color getColor() {
        return color;
    }

    public abstract Prototype clone();


}
