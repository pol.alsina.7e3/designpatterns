package figuraGeometrica;

import prototype.Prototype;

import java.awt.*;

public class Rectangulo extends FiguraGeometrica {

    protected double base;
    protected double altura;
    public Rectangulo(String nombre, Color color, double base, double altura) {
        super(nombre, color);
        this.base = base;
        this.altura = altura;
    }

    private Rectangulo(Rectangulo toCopy){
        super(toCopy.nombre, toCopy.color);
        this.base = toCopy.base;
        this.altura = toCopy.altura;
    }

    @Override
    public Prototype clone() {
        return new Rectangulo(this);
    }

    @Override
    public String calculateArea() {
        double area = base*altura;
        return "El area del rectangulo " + nombre + " es: " + area;

    }

    @Override
    public String toString() {
        return "Rectangulo{" +
                "nombre='" + nombre + '\'' +
                ", color=" + color +
                ", base=" + base +
                ", altura=" + altura +
                '}';
    }
}
