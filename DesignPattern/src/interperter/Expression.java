package interperter;


public interface Expression {
    int interpret();
}
