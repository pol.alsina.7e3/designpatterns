package interperter;


import org.junit.Test;

import java.util.List;

public class MainInterperter {
    public static void main(String[] args) {

    }
    @Test
    public void testParse() throws Exception {
        String input="5 2 1 + *";
        ExpressionParser expressionParser=new ExpressionParser();
        int result=expressionParser.parse(input);
        System.out.println("Final result: "+result);
    }
    @Test
    public void testParse1() throws Exception {
        String input="1 30 3 + *";
        ExpressionParser expressionParser=new ExpressionParser();
        int result=expressionParser.parse(input);
        System.out.println("Final result: "+result);
    }

    @Test
    public void testParse2() throws Exception {
        String input = "1 33 66 1 + - -";
        ExpressionParser expressionParser = new ExpressionParser();
        int result = expressionParser.parse(input);
        System.out.println("Final result: " + result);
    }
}
