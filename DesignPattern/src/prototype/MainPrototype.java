package prototype;

import figuraGeometrica.Circulo;
import figuraGeometrica.Cuadrado;
import figuraGeometrica.Rectangulo;

import java.awt.*;

public class MainPrototype {
    public static void main(String[] args) {
        Circulo c1 = new Circulo("Parra", Color.BLUE, 3);
        Circulo cCopia = (Circulo) c1.clone();
        cCopia.setNombre("Clone");
        System.out.println(c1);
        System.out.println(cCopia);


        Rectangulo r1 = new Rectangulo("Alonso",Color.cyan,3,5);
        Rectangulo rCopia = (Rectangulo) r1.clone();
        System.out.println(r1);
        System.out.println(rCopia);

        Cuadrado cuadrado1 = new Cuadrado("32",Color.cyan,3);
        Cuadrado cuadradoClone = (Cuadrado) cuadrado1.clone();
        System.out.println(cuadrado1);
        System.out.println(cuadradoClone);
    }
}
